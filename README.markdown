Introduction
------------

AI vs AI is a platform that user may view 2 AI for Othello game compete with each other. User may submit his/her own AI code as well.

This project is developed by [Liang Sun][liang] and [Yangjing Zhang][yangjing] on Oct 27~28, 2012 as a 27-hour project for [Pythonic Hackthon 2012 China][hackthon]. Back-end is developed by Liang Sun while front-end developed by Yangjing Zhang.

The project is depoloyed on [Sina's SAE][demo]. Why not clik the link and have a look?

[liang]: http://liangsun.org "Liang Sun"
[yangjing]: mailto:zhangyangjing@gmail.com "Yangjing Zhang"
[hackthon]: http://www.douban.com/event/17299206/ "Pythonic Hackthon"
[demo]: http://aivsai.sinaapp.com "AI vs AI"

Component
---------

+ web.py
+ flexgrid
+ codemirror
+ MySQL



#encoding:utf-8
import os, sys, StringIO, random, json
from hashlib import sha1
import sae, web
from web import form
        
from config import db, urls
import users, session

app_root = os.path.dirname(__file__)
templates_root = os.path.join(app_root, 'templates')
render = web.template.render(templates_root)

vemail = form.regexp(r'[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}', 'Please enter a valid email address')
login_form = form.Form(
    form.Textbox('email', 
        form.notnull, vemail,
        description='Your email:'),
    form.Password('password', 
        form.notnull,
        description='Your password:'),
                
    form.Button('submit', type='submit', value='Login'),
    validators = [
        form.Validator('Incorrect email / password combination.', 
            lambda i: users.is_correct_password(i.email, i.password))
    ]
)

signup_form = form.Form(
    form.Textbox('email', 
        form.notnull, vemail,
        form.Validator('This email address is already taken.', 
        lambda x: users.is_email_available(x)),
        description='Your email:'),
    form.Password('password', 
        form.notnull,
        form.Validator('Your password must at least 5 characters long.', 
        lambda x: users.is_valid_password(x)),
        description='Choose a password:'),
    form.Textbox('nickname', 
        form.notnull,
        description='Choose a nickname:'),
                
    form.Button('submit', type='submit', value='Register'),
)

class Index:        
    def GET(self):
        return render.index(is_logged=session.is_logged())

class Login:
    def GET(self):
        return render.login(login_form())
    
    def POST(self):
        f = self.form()
        if not f.validates(web.input(_unicode=False)):
            return render.login(form=f)
        else:
            session.login(f.d.email)
            raise web.seeother('/')
            #raise web.seeother(session.get_last_visited_url())
    
    def form(self):
        return login_form()
    
class Signup:
    def GET(self):
        return render.signup(signup_form())
    
    def POST(self):
        f = self.form()
        if not f.validates(web.input(_unicode=False)):
            return render.signup(form=f)
        else:
            users.create_account(f.d.email, f.d.password, f.d.nickname)
            session.login(f.d.email)
            raise web.seeother('/')
    
    def form(self):
        return signup_form()

class Logout:
    def GET(self):
        session.logout()
        raise web.seeother('/')
 
class New:
    def GET(self):
        return render.new()
    @session.login_required
    def POST(self):
        i = web.input()
        n = db.insert('robot', code=i.code, uid=session.get_user_id())

        code = i.code
        tmp = self.is_valid_code(code)
        if tmp[0]:
            code = self.trans(code, n)

            tmp = self.tested(code)
            if tmp[0]:
                db.update('robot', vars=dict(rid=n), where='rid=$rid', tested=1)

            else:
                return tmp[1]

        else:
            return "Code not valid.%s" % tmp[1]

        raise web.seeother('/')

    def tested(self, code):
        try:
            exec code

            try:
                cm = []
                for i in range(8):
                    cm.append([0]*8)

                cm[3][3] = cm[4][4] = 1 
                cm[3][4] = cm[4][3] = -1

                #return (False, code)
                pos = r0.move(cm)

                #return (False, "%d, %d" % pos)
                if not((pos[0] == 2 and pos[1] == 4) or 
                        (pos[0] == 3 and pos[1] == 5) or
                        (pos[0] == 4 and pos[1] == 2) or
                        (pos[0] == 5 and pos[1] == 3)):
                    return (False, 'Algorithm error.')

            except:
                return (False, "move function compiled error")
            
            return (True, "")
        except:
            return (False, "Compiled error.")

    def is_valid_code(self, code):
        lines = code.splitlines()
        if len(lines) < 2:
            return (False, "code lines less than 2")
        if lines[0][:4] != 'def ':
            return (False, "not start with def")
        for i in xrange(1, len(lines)):
            if len(lines[i].strip()) > 0 and lines[i][0] not in (" ", "\t"):
                return (False, "%s not valid" % lines[i])

        return (True,"")

    def trans(self, code, n):
        lines = code.splitlines()
        ans = "class Robot%d:\n" % n
        p = lines[0].find('(')
        ans += "\t%sself, %s\n" % (lines[0][:p+1], lines[0][p+1:])
        for i in xrange(1, len(lines)):
            ans += "\t%s\n" % lines[i]

        ans += "r0 = Robot%d()" % n
        return ans

class Pk:
    def GET(self, black_rid, white_rid):
        brobot = web.listget(db.select('robot', vars=dict(rid=black_rid), where='rid=$rid'), 0, {})
        if len(brobot) == 0:
            return "Black AI not exists."
        if brobot.tested < 1:
            return "Black AI not valid."

        wrobot = web.listget(db.select('robot', vars=dict(rid=white_rid), where='rid=$rid'), 0, {})
        if len(wrobot) == 0:
            return "White player not exists."
        if wrobot.tested < 1:
            return "White AI not valid."
        
        act = web.listget(db.select('activity',
            vars = dict(black_rid=black_rid,
                white_rid=white_rid),
            where = 'black_rid = $black_rid and white_rid = $white_rid'), 0, {})

        if len(act) == 0 or not act.get('result', False):
            n = db.insert('activity', black_rid=black_rid, white_rid=white_rid)
            result = self.pk(n, black_rid, white_rid)
            if result[0]:
                db.update('activity', vars=dict(aid=n),
                        where = 'aid=$aid', result=result[1])
                
                if result[1] > 0:
                    if brobot.credit < wrobot.credit:
                        db.update('robot', vars=dict(rid=black_rid), where='rid=$rid', credit=wrobot.credit+1)
                        db.update('robot', vars=dict(rid=white_rid), where='rid=$rid', credit=brobot.credit-1)
                    else:
                        db.update('robot', vars=dict(rid=black_rid), where='rid=$rid', credit=brobot.credit+1)
                        db.update('robot', vars=dict(rid=white_rid), where='rid=$rid', credit=wrobot.credit-1)

                if result[1] < 0:
                    if brobot.credit > wrobot.credit:
                        db.update('robot', vars=dict(rid=black_rid), where='rid=$rid', credit=wrobot.credit-1)
                        db.update('robot', vars=dict(rid=white_rid), where='rid=$rid', credit=brobot.credit+1)
                    else:
                        db.update('robot', vars=dict(rid=black_rid), where='rid=$rid', credit=brobot.credit-1)
                        db.update('robot', vars=dict(rid=white_rid), where='rid=$rid', credit=wrobot.credit+1)
                
            else:
                return result[1]
        else:
            n = act.get('aid', False)

        hs = db.select('history',
                vars=dict(aid=n),
                where = 'aid=$aid')
        ans = {}
        for h in hs:
            ans[str(h.step)] = dict(id=h.hid,who=h.who,x=h.row,y=h.col)

        return json.dumps(ans)

    def pk(self, aid, b_rid, w_rid):
        if self.extract(b_rid, "self.br") == False:
            return (False, "Extract black AI failed.")
        #br = Black Robot

        if self.extract(w_rid, "self.wr") == False:
            return (False, "Extract white AI failed.")
        #wr = White Robot

        step = 1
        over = False
        onbye = False
        black = True
        cm = self.init_map()
        while not over:
            if self.has_pos(cm):
                onbye = False

                if black:
                    pos = self.br.move(cm)
                    db.insert('history', aid=aid, step=step, row=pos[0], col=pos[1], who=1)
                    if not self.is_valid_pos(pos, cm):
                        return (True, -1)
                        
                else:
                    pos = self.wr.move(cm)
                    db.insert('history', aid=aid, step=step, row=pos[0], col=pos[1], who=0)
                    if not self.is_valid_pos(pos, cm):
                        return (True, 1)

                cm = self.put(cm, pos)
                step += 1

            else:
                if onbye:
                    over = True
                else:
                    onbye = True

            cm = self.reverse(cm)
            black = not black

        if not black:
            cm = self.reverse(cm)

        (bn, wn) = self.calculate(cm)
        return (True, bn-wn)

    def extract(self, rid, robot_name):
        rid = int(rid)
        robot = web.listget(db.select('robot', vars=dict(rid=rid), where="rid=$rid"), 0, {})

        if not robot.get('code', False):
            return False
        
        code = robot.code

        lines = code.splitlines()
        ans = "class Robot%d:\n" % rid
        p = lines[0].find('(')
        ans += "\t%sself, %s\n" % (lines[0][:p+1], lines[0][p+1:])
        for i in xrange(1, len(lines)):
            ans += "\t%s\n" % lines[i]

        ans += "%s = Robot%d()\n" % (robot_name, rid)

        try:
            exec ans
        except:
            return False

        return ans
 

    def is_valid_pos(self, pos, cm):
        if len(pos) < 2 or (pos[0] == -1 and pos[1] == -2):
            return self.onbye(cm)
        
        if not self.in_board(pos[0], pos[1]) or cm[pos[0]][pos[1]] != 0:
            return False

        c = 0
        x, y = pos
        for dx in range(-1, 2):
            for dy in range(-1, 2):
                if dx == 0 and dy == 0:
                    continue

                if self.in_board(x+dx, y+dy) and -1 == cm[x+dx][y+dy]:
                    dz=1
                    while self.in_board(x+dx*dz, y+dy*dz) and -1 == cm[x+dx*dz][y+dy*dz]:
                        dz += 1

                    if self.in_board(x+dx*dz, y+dy*dz) and 1 == cm[x+dx*dz][y+dy*dz]:
                        c += dz-1

        return c > 0


    def put(self, cm, pos):
        if 0 == len(pos):
            return
    
        ans = []
        for i in range(8):
            tmp = []
            for j in range(8):
                tmp.append(cm[i][j])
    
            ans.append(tmp)
    
        x, y = pos
        ans[x][y] = 1
    
        for dx in range(-1, 2):
            for dy in range(-1, 2):
                if 0 == dx and 0 == dy:
                    continue
                if self.in_board(x+dx, y+dy) and -1 == ans[x+dx][y+dy]:
                    dz = 1
                    while self.in_board(x+dx*dz, y+dy*dz) and -1 == ans[x+dx*dz][y+dy*dz]:
                        dz += 1
    
                    if self.in_board(x+dx*dz, y+dy*dz) and 1 == ans[x+dx*dz][y+dy*dz]:
                        for k in range(1, dz):
                            ans[x+dx*k][y+dy*k] = 1
    
        return ans

    def reverse(self, cm):
        ans = []
        for i in range(8):
            tmp = []
            for j in range(8):
                if cm[i][j] == 0:
                    tmp.append(0)
                else:
                    tmp.append(-cm[i][j])
            ans.append(tmp)
        return ans


    def calculate(self, cm):
        bn = 0
        wn = 0
        for i in range(8):
            for j in range(8):
                if cm[i][j] == 1:
                    bn += 1
                elif cm[i][j] == -1:
                    wn += 1

        return (bn, wn)

    def onbye(self, cm):
        for i in range(8):
            for j in range(8):
                if self.is_valid_pos((i,j), cm):
                    return False

        return True

    def has_pos(self, cm):
        return not self.onbye(cm)

    def in_board(self, x, y):
        return x >= 0 and x < 8 and y >= 0 and y < 8

    def init_map(self):
        cm = []
        for i in range(8):
            cm.append([0]*8)

        cm[3][3] = cm[4][4] = 1
        cm[3][4] = cm[4][3] = -1
        return cm

class All:
    def GET(self):
        robots = db.query('select r.*, u.nickname from robot r left join user u on u.uid = r.uid order by r.credit desc')
        ans = []
        for robot in robots:
            if robot.tested:
                ans.append(dict(id=robot.rid,author=robot.nickname,name="robot%d"%robot.rid))

        return json.dumps(ans)

app = web.application(urls, globals())

session.add_sessions_to_app(app)
application = sae.create_wsgi_app(app.wsgifunc())

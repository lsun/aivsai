#encoding:utf-8
import sae, web
from sae.const import *

db = web.database(dbn='mysql', user=MYSQL_USER, pw=MYSQL_PASS, host=MYSQL_HOST, port=(int)(MYSQL_PORT), db=MYSQL_DB)

urls = (
    '/', 'Index',
    '/new', 'New',
    '/list', 'All',
    '/login', 'Login',
    '/signup', 'Signup',
    '/logout', 'Logout',
    '/pk/(\d+)/(\d+)', 'Pk',
)



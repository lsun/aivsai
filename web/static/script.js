
var CHESSMAN_TYPE_BLACK = 0;
var CHESSMAN_TYPE_WHITE = 1;

var QIPAN_LENGHT = 9;


function Chessman(game, type, locX, locY) {
    this.game = game;
    this.type = type;
    this.x = locX;
    this.y = locY;

    this.curX = 0;
    this.curY = 0;
    this.stepX = 0;
    this.stepY = 0;
    this.toX = 0; // not pix
    this.toY = 0; // not pix
}
Chessman.prototype.update = function() {
    var hready = false;
    var vready = false;

    if ((this.stepX > 0  & this.curX + this.stepX >= this.x) || (this.stepX < 0 && this.curX + this.stepX <= this.x)) {
        this.curX = this.x;
        hready = true;  
    } else {
        this.curX += this.stepX;
    }

    if ((this.stepY > 0 && this.curY + this.stepY >= this.y) || (this.stepY < 0 && this.curY + this.stepY <= this.y)) {
        this.curY = this.y;
        vready = true;
    } else {
        this.curY += this.stepY;
    }

    if (true == hready && true == vready) {
        this.game.moveFinish(this);
    }
}

function Game() {
    this.img_bg = null;
    this.img_black = null;
    this.img_white = null;

    this.canvas = null;
    this.contex= null;
    this.time_int = null; 

    this.chessmans_moving = new Array();    
    this.chessmans = new Array();
    for (x = 0; x < QIPAN_LENGHT; x++) {
        for (y = 0; y < QIPAN_LENGHT; y++) {
            this.setChessman(x, y, undefined);
        }    
    }
}
Game.prototype.getChessman = function(x, y) {
    return this.chessmans[x+y*QIPAN_LENGHT];
}
Game.prototype.setChessman = function(x, y, value) {
    this.chessmans[x+y*QIPAN_LENGHT] = value;
}
Game.prototype.getPixLoc = function(x, y) {
    var v = new Array();  
    v['x'] = x * 75;
    v['y'] = y * 75;

    return v;
}
Game.prototype.refreshCanvas = function() {
    var chessman, img, x, y;

    this.contex.drawImage(this.img_bg,0,0,600,600); 

    for (i = 0; i < QIPAN_LENGHT*QIPAN_LENGHT; i++) {
        chessman = this.chessmans[i];
        if (undefined != chessman) {
            x = chessman.x;
            y = chessman.y;
            img = chessman.type==CHESSMAN_TYPE_BLACK?this.img_black:this.img_white;
            this.contex.drawImage(img, x+3, y+3, 70, 70); 
        } 
    }
    for (i = 0; i<this.chessmans_moving.length; i++) {
        chessman = this.chessmans_moving[i];
        chessman.update();
        x = chessman.curX;
        y = chessman.curY;
        img = chessman.type==CHESSMAN_TYPE_BLACK?this.img_black:this.img_white;
        this.contex.drawImage(img, x+3, y+3, 70, 70); 
    }
}
Game.prototype.moveFinish = function(chessman) {
    this.setChessman(chessman.toX, chessman.toY, chessman);

    for (i = 0; i < this.chessmans_moving.length; i++) {
        if(chessman == this.chessmans_moving[i]) {
            this.chessmans_moving.splice(i, 1);
        }
    }

    // TODO: call back if need
}
Game.prototype.start = function () {
    this.img_bg = new Image();
    this.img_black = new Image();
    this.img_white = new Image();
    this.img_bg.src = "static/images/board.png";
    this.img_black.src = "static/images/black.png";
    this.img_white.src = "static/images/white.png";

    this.canvas=document.getElementById("myCanvas");
    this.contex = this.canvas.getContext("2d"); 
    this.contex.strokeStyle = "#00f"; 
    this.contex.strokeRect(0, 0, this.canvas.width, this.canvas.height);
    this.time_int=setInterval("refresh()",300);

    this.add(CHESSMAN_TYPE_BLACK, 3, 4);
    this.add(CHESSMAN_TYPE_BLACK, 4, 3);
    this.add(CHESSMAN_TYPE_WHITE, 3, 3);
    this.add(CHESSMAN_TYPE_WHITE, 4, 4);
    this.refreshCanvas();
    
    $("#btnPreviousStep").attr("disabled", true);
    $("#wCent").text("2");
    $("#bCent").text("2");
    
}
Game.prototype.reset = function() {
    this.chessmans_moving = new Array(); 
    for (x = 0; x < QIPAN_LENGHT; x++) {
        for (y = 0; y < QIPAN_LENGHT; y++) {
            this.setChessman(x, y, undefined);
        }    
    }
}
Game.prototype.in_board = function(x, y) {
    return x >= 0 && x < 8 && y >= 0 && y < 8;
}
Game.prototype.revert = function(type, x, y) {
    this.setChessman(x, y, undefined);
    
}
Game.prototype.revert = function(type, x, y) {
    this.setChessman(x, y, undefined);

    var stackElement = curStack.pop();
    for (i = 0; i < stackElement.length; ++i) {
        this.getChessman(stackElement[i][0], stackElement[i][1]).type = !this.getChessman(stackElement[i][0], stackElement[i][1]).type;
    }
}
Game.prototype.add = function(type, x, y) {
    var pixX, pixY;

    pixX = this.getPixLoc(x, y)['x'];
    pixY = this.getPixLoc(x, y)['y'];

    var chessman = new Chessman(this, type, pixX, pixY);
    this.setChessman(x, y, chessman); 

    var stackElement = [];
    var dx, dy, dz;
    for (dx = -1; dx < 2; ++dx) {
        for (dy = -1; dy < 2; ++dy) {
            if (0 == dx && 0 == dy) {
                continue;
            }
            if (this.in_board(x+dx, y+dy) && this.getChessman(x+dx, y+dy) != type) {
                dz = 1;
                while (this.in_board(x+dx*dz, y+dy*dz) && undefined != this.getChessman(x+dx*dz, y+dy*dz) && this.getChessman(x+dx*dz, y+dy*dz).type != type) {
                    ++dz;
                }
                if (this.in_board(x+dx*dz, y+dy*dz) && undefined != this.getChessman(x+dx*dz, y+dy*dz) && this.getChessman(x+dx*dz, y+dy*dz).type == type) {
                    for (i = 1; i < dz; ++i) {
                        this.getChessman(x+dx*i, y+dy*i).type = !this.getChessman(x+dx*i, y+dy*i).type;
                        stackElement.push([x+dx*i, y+dy*i]);
                    }
                }
            }
        }
    }
    curStack.push(stackElement);
}
Game.prototype.move = function(fromX, fromY, toX, toY) {
    var chessman = this.getChessman(fromX, fromY); 
    if (null == chessman) {
        log("error when move");  
        return;
    }

    chessman.toX = toX;
    chessman.toY = toY;
    chessman.curX = chessman.x;
    chessman.curY = chessman.y
        chessman.x = this.getPixLoc(toX, toY)['x'];
    chessman.y = this.getPixLoc(toX, toY)['y'];
    //chessman.stepX = (this.getPixLoc(toX, toY)['x'] - this.getPixLoc(fromX, fromY)['x']);
    //chessman.stepY = this.getPixLoc(toX, toY)['y'] - this.getPixLoc(fromX, fromY)['y'];
    chessman.stepX = this.getPixLoc(toX, toY)['x'] - this.getPixLoc(fromX, fromY)['x'] > 0 ? 7 : -7;
    chessman.stepY = this.getPixLoc(toX, toY)['y'] - this.getPixLoc(fromX, fromY)['y'] > 0 ? 7 : -7;
    this.chessmans_moving.push(chessman);

    this.setChessman(fromX, fromY, undefined);
}
Game.prototype.getAnsyle = function() {
    var wCount = 0;
    var bCount = 0;
    for (x = 0; x < QIPAN_LENGHT; x++) {
        for (y = 0; y < QIPAN_LENGHT; y++) {
            var chessman = this.getChessman(x, y);
            if (undefined == chessman)
                continue;
            if (CHESSMAN_TYPE_BLACK == chessman.type) {
                bCount++;
            } else {
                wCount++;
            }
        }
    }
    
    return {"black":bCount, "white":wCount};
}

function refresh() {
    game.refreshCanvas();
}

function log(str) {
    console.log(str);
}


function switchPanal(panal) {
    switch (panal) {
        case "list":
            $('#codeCommite').hide();
            $('#codeLists').show();
            $('#gamePlay').hide();
            $('#waitting').hide();
            break;
        case "commit":
            $('#codeCommite').show();
            $('#codeLists').hide();
            $('#gamePlay').hide();
            $('#waitting').hide();
            break;
        case "game":
            $('#codeCommite').hide();
            $('#codeLists').hide();
            $('#gamePlay').show();
            $('#waitting').hide();
            break;
        case "waitting":
            $('#codeCommite').hide();
            $('#codeLists').hide();
            $('#gamePlay').hide();
            $('#waitting').show();
            var opts = {
                lines: 13, // The number of lines to draw
                length: 30, // The length of each line
                width: 14, // The line thickness
                radius: 38, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 56, // The rotation offset
                color: '#000', // #rgb or #rrggbb
                speed: 1.5, // Rounds per second
                trail: 80, // Afterglow percentage
                shadow: true, // Whether to render a shadow
                hwaccel: true, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: 'auto', // Top position relative to parent in px
                left: 'auto' // Left position relative to parent in px
            };
            var target = document.getElementById('waitting');
            spinner = new Spinner(opts).spin(target);
            break;
    }
}

function clearList() {

}

function generateList() {
    switchPanal('waitting');
    $.get("list", null, function (data, textStatus) {
        if ("success" != textStatus) {
            alert("bad http return : " + textStatus);
            return;
        }

        switchPanal('list');
        createList(eval(data));
    }
    );
}
function createList(datas) {
    var str, item, id, name, author;

    var head2 = '<table class="flexme2"> \
                <thead> \
                <tr>\
                <th width="180">Name</th>\
                <th width="73">Author</th>\
                <th width="0">id</th>\
                </tr>\
                </thead>\
                <tbody>';
    var header1 = '<table class="flexme1"> \
                  <thead> \
                  <tr>\
                  <th width="180">Name</th>\
                  <th width="73">Author</th>\
                  <th width="0">id</th>\
                  </tr>\
                  </thead>\
                  <tbody>';

    str = " ";
    for (i = 0; i < datas.length; i++) {
        item = datas[i]; 
        id = item['id'];
        name = item['name'];
        author = item['author'];

        str += '<tr>';
        str += '<td class="name">' + name + '</td>'; 
        str += '<td class="author">' + author + '</td>';
        str += '<td class="id">' + id + '</td>';
        str += '</tr>'; 
    }
    str += '</tbody></table>';



    $('#list1').html(header1 + str);
    $('#list2').html(head2 + str);
    list1 = $('.flexme1').flexigrid({width: 280,height: 400, resizable: false, singleSelect: true});
    list2 = $('.flexme2').flexigrid({width: 280,height: 400, resizable: false, singleSelect: true});
}
function selectList() {
    if (0==$('.trSelected',list1).length || 0==$('.trSelected',list1).length) {
        alert("please seclect the robots");
        return
    }

    var id1 = $('.id', $('.trSelected',list1)[0])[0].innerText;
    var id2 = $('.id', $('.trSelected',list2)[0])[0].innerText;
    
    $("#bName").text("Name:" + $('.name', $('.trSelected',list1)[0])[0].innerText);
    $("#wName").text("Name:" + $('.name', $('.trSelected',list2)[0])[0].innerText);
    $("#bAuthor").text("Author:" + $('.author', $('.trSelected',list1)[0])[0].innerText);
    $("#wAuthor").text("Author:" + $('.author', $('.trSelected',list2)[0])[0].innerText);
    
    generateGame(id1, id2);
}

function generateGame(id1, id2) {  
    switchPanal('waitting');
    $.getJSON("pk/"+id1+"/"+id2, null, function (datas, textStatus) {
        if ("success" != textStatus) {
            alert("bad http return: " + textStatus);
            return;
        }

        switchPanal('game');
        runGame(datas);
    }
    );
}
function runGame(data) {
log(data);
    gameData = data;
    curStep = 1;
    curStack = [];
    game = new Game();
    game.start();
}
function previousStep() {
    var a = curStep;
    var b = gameData.length;
    var step = gameData[''+(--curStep)];
    game.revert(step['who']==0?CHESSMAN_TYPE_BLACK:CHESSMAN_TYPE_WHITE, step['x'], step['y']);

    game.refreshCanvas();
    if (1 == curStep) {
        $("#btnPreviousStep").attr("disabled", true);
    }
    $("#btnNextStep").attr("disabled", false);
    $("#btnStartAutoPlay").attr("disabled", false);
}
function nextStep() {
    var a = curStep;
    var b = gameData.length;
    var step = gameData[''+curStep++];
    game.add(step['who']==0?CHESSMAN_TYPE_BLACK:CHESSMAN_TYPE_WHITE, step['x'], step['y']);
    game.refreshCanvas();
    
    if (false == isAutoPlay())
    	$("#btnPreviousStep").attr("disabled", false);
        
    $("#wCent").text(""+game.getAnsyle()["white"]);
    $("#bCent").text(""+game.getAnsyle()["black"]);
    
    if (curStep == Object.keys(gameData).length + 1) {
        if (true == isAutoPlay()) {
            stopAutoPlay();
        }

        alert("GAME OVER!\nWhite:"+game.getAnsyle()["white"]+"\nBlack:"+game.getAnsyle()["black"]);
        
        $("#btnNextStep").attr("disabled", true);
        $("#btnStartAutoPlay").attr("disabled", true);
    }
}

var autoplay_time_int = undefined;
var isAutoPlay_ = false;
function startAutoPlay() {
    isAutoPlay_ = true;
    autoplay_time_int = setInterval("nextStep()",1000);
    $("#btnNextStep").attr("disabled", true);
    $("#btnPreviousStep").attr("disabled", true);
    $("#btnStartAutoPlay").text('Stop AutoPlay');
    $("#btnStartAutoPlay").attr('onclick', '').unbind('click').click(stopAutoPlay);
}
function stopAutoPlay() {
    isAutoPlay_ = false;
    clearInterval(autoplay_time_int);
    $("#btnNextStep").attr("disabled", false);
    $("#btnPreviousStep").attr("disabled", false);
    $("#btnStartAutoPlay").text('Start AutoPlay');
    $("#btnStartAutoPlay").attr('onclick', '').unbind('click').click(startAutoPlay);
}
function isAutoPlay() {
    return isAutoPlay_;
}

function showCommit() {
    switchPanal('commit');
}
function commitCode() {
    $.post("new", 
            { "code":codeMirror.getValue()},
            function (result){
                switchPanal('list');
            }
          );
}


$(function() {
    // CodeMirror
    codeMirror = CodeMirror.fromTextArea(document.getElementById("code"), 
        {
               mode: {name: "python", version: 2, singleLineStringErrors: false},
               height:"100%",
               lineNumbers: true,
               indentUnit: 4,
               tabMode: "shift",
               matchBrackets: true
        }
        );

    game = new Game();
    switchPanal('list');
    generateList();
});

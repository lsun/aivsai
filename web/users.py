import web
from config import db

import random
from hashlib import sha1

def hash_password(password, salt):
    return sha1(password + salt).hexdigest()[:64]

def create_account(email, password, nickname):
    salt = "".join(chr(random.randint(33,127)) for x in xrange(64))
    password = hash_password(password, salt)

    db.insert('user', email=email, password=password, salt=salt, nickname=nickname)
    
def get_user_by_email(email):
    return web.listget(
        db.select('user', vars=dict(email=email), 
            where='email = $email'), 0, {})

def is_email_available(email):
    return not db.select(
        'user', 
        vars = dict(email=email),
        what = 'count(uid) as c', 
        where = 'email = $email')[0].c

def is_valid_password(password):
    return len(password) >= 5

def is_correct_password(email, password):
    user = get_user_by_email(email)
    return user.get('password', False) == hash_password(password, user.get('salt', False))

def update(uid, **kw):
    db.update('user', vars=dict(uid=uid), where='uid = $uid', **kw)

def get_user_by_id(uid):
    return web.listget(
        db.select('user', vars=dict(uid=uid), 
            where='uid = $uid'), 0, {})

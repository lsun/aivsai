#encoding:utf-8
def in_board(row, col):
    return row >=0 and row < 8 and col >=0 and col < 8

def display(cur_map):
    for i in range(8):
        print cur_map[i]

class Robot:
    def move(self, cm):
        def in_board(row, col):
            return row >=0 and row < 8 and col >=0 and col < 8

        def is_valid_pos(pos, cm):
            x, y = pos
            if not in_board(x, y):
                return False
            if cm[pos[0]][pos[1]] != 0:
                return False
            c=0
            for dx in range(-1, 2):
                for dy in range(-1, 2):
                    if dx == 0 and dy == 0:
                        continue
                    if in_board(x+dx, y+dy) and cm[x+dx][y+dy] == -1:
                        dz = 1
                        while in_board(x+dx*dz, y+dy*dz) and cm[x+dx*dz][y+dy*dz] == -1:
                            dz += 1

                        if in_board(x+dx*dz, y+dy*dz) and cm[x+dx*dz][y+dy*dz] == 1:
                            c+=dz-1
            return c

        def get_all_pos(cm):
            ans = []
            for i in range(8):
                for j in range(8):
                    if is_valid_pos((i,j), cm):
                        ans.append((i,j))
            return ans

        def reverse(cur_map):
            ans = []
            for i in range(8):
                tmp = []
                for j in range(8):
                    if 0 == cur_map[i][j]:
                        tmp.append(0)
                    else:
                        tmp.append(-cur_map[i][j])
        
                ans.append(tmp)
            return ans
            
        def put(cur_map, pos):
            if 0 == len(pos):
                return
        
            ans = []
            for i in range(8):
                tmp = []
                for j in range(8):
                    tmp.append(cur_map[i][j])
        
                ans.append(tmp)
        
            x, y = pos
            ans[x][y] = 1
        
            for dx in range(-1, 2):
                for dy in range(-1, 2):
                    if 0 == dx and 0 == dy:
                        continue
                    if in_board(x+dx, y+dy) and -1 == ans[x+dx][y+dy]:
                        dz = 1
                        while in_board(x+dx*dz, y+dy*dz) and -1 == ans[x+dx*dz][y+dy*dz]:
                            dz += 1
        
                        if in_board(x+dx*dz, y+dy*dz) and 1 == ans[x+dx*dz][y+dy*dz]:
                            for k in range(1, dz):
                                ans[x+dx*k][y+dy*k] = 1
        
            return ans
                   
        def move_one(cm, deep):
            ans = ()
            m = 0
            if deep == 0:
                for i in range(8):
                    for j in range(8):
                        if cm[i][j] == 0:
                            tmp = is_valid_pos((i,j), cm)
                            if tmp > m:
                                #print tmp #for debug
                                m = tmp
                                ans = (i, j)
                
                return (m, ans)

            
            poses = get_all_pos(cm)
            for pos in poses:
                c = is_valid_pos(pos, cm)
                nm = put(cm, pos)
                nm = reverse(nm)

                d, e = move_one(nm, deep-1)
                if c-d > m:
                    m = c-d
                    ans = pos

            return (m, ans)
        a, b = move_one(cm, 2)
        return b

    
def reverse(cur_map):
    ans = []
    for i in range(8):
        tmp = []
        for j in range(8):
            if 0 == cur_map[i][j]:
                tmp.append(0)
            else:
                tmp.append(-cur_map[i][j])

        ans.append(tmp)
    return ans
    
def put(cur_map, pos):
    if 0 == len(pos):
        return

    ans = []
    for i in range(8):
        tmp = []
        for j in range(8):
            tmp.append(cur_map[i][j])

        ans.append(tmp)

    x, y = pos
    ans[x][y] = 1

    for dx in range(-1, 2):
        for dy in range(-1, 2):
            if 0 == dx and 0 == dy:
                continue
            if in_board(x+dx, y+dy) and -1 == ans[x+dx][y+dy]:
                dz = 1
                while in_board(x+dx*dz, y+dy*dz) and -1 == ans[x+dx*dz][y+dy*dz]:
                    dz += 1

                if in_board(x+dx*dz, y+dy*dz) and 1 == ans[x+dx*dz][y+dy*dz]:
                    for k in range(1, dz):
                        ans[x+dx*k][y+dy*k] = 1

    return ans

if __name__ == "__main__":
    r = Robot()
    cm = []
    for i in range(8):
        cm.append([0]*8)

    cm[3][3] = cm[4][4] = 1
    cm[3][4] = cm[4][3] = -1
    
    for i in range(8):
        pos = r.move(cm)
        print '+'*30
        print pos
        cm = put(cm, pos)
        display(cm)
        cm = reverse(cm)
        print '-'*30
    

#encoding:utf-8
def in_board(row, col):
    return row >=0 and row < 8 and col >=0 and col < 8

def display(cur_map):
    for i in range(8):
        print cur_map[i]

class Robot:
    def move(self, current_map):
        #current_map is 64*64 matrix
        #-1 means occupied by adversary, 1 means occupied by user, 0 means not occppied.

        #return the postion of this move (row, col). row, col ~ [0, 7]

        val = []

        def in_board(row, col):
            return row >=0 and row < 8 and col >=0 and col < 8
        
        def cal(cur_map, row, col):

            ans = 0
            for dx in range(-1, 2):
                for dy in range(-1, 2):
                    if 0 == dx and 0 == dy:
                        continue
                    if in_board(row+dx, col+dy) and -1 == cur_map[row+dx][col+dy]:
                        dz = 1
                        while in_board(row+dx*dz, col+dy*dz) and -1 == cur_map[row+dx*dz][col+dy*dz]:
                            dz += 1
                            
                        if in_board(row+dx*dz, col+dy*dz) and 1 == cur_map[row+dx*dz][col+dy*dz]:
                            ans += dz - 1

            return ans

        ans = []

        for i in range(8):
            for j in range(8):
                if current_map[i][j] == 0:
                    tmp = cal(current_map, i, j)
                    if tmp > 0:
                        print tmp #for debug
                        ans.append((i, j))
                
        import random

        return ans[random.randint(0, len(ans) - 1)]

def reverse(cur_map):
    ans = []
    for i in range(8):
        tmp = []
        for j in range(8):
            if 0 == cur_map[i][j]:
                tmp.append(0)
            else:
                tmp.append(-cur_map[i][j])

        ans.append(tmp)
    return ans
    
def put(cur_map, pos):
    if 0 == len(pos):
        return

    ans = []
    for i in range(8):
        tmp = []
        for j in range(8):
            tmp.append(cur_map[i][j])

        ans.append(tmp)

    x, y = pos
    ans[x][y] = 1

    for dx in range(-1, 2):
        for dy in range(-1, 2):
            if 0 == dx and 0 == dy:
                continue
            if in_board(x+dx, y+dy) and -1 == ans[x+dx][y+dy]:
                dz = 1
                while in_board(x+dx*dz, y+dy*dz) and -1 == ans[x+dx*dz][y+dy*dz]:
                    dz += 1

                if in_board(x+dx*dz, y+dy*dz) and 1 == ans[x+dx*dz][y+dy*dz]:
                    for k in range(1, dz):
                        ans[x+dx*k][y+dy*k] = 1

    return ans

if __name__ == "__main__":
    r = Robot()
    cm = []
    for i in range(8):
        cm.append([0]*8)

    cm[3][3] = cm[4][4] = 1
    cm[3][4] = cm[4][3] = -1
    
    for i in range(8):
        pos = r.move(cm)
        print '+'*30
        print pos
        cm = put(cm, pos)
        display(cm)
        cm = reverse(cm)
        print '-'*30
    
